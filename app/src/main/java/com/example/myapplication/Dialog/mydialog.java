package com.example.myapplication.Dialog;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.myapplication.Adapter.CustomGridAdapter;
import com.example.myapplication.R;


public class mydialog extends DialogFragment implements View.OnClickListener
{

    Button bt;
    ImageView iv;
    CustomGridAdapter cg;
    private int imageResource;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup          container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.my_dialog,null);
        bt=(Button)view.findViewById(R.id.button);
        bt.setOnClickListener(this);
        iv= (ImageView) view.findViewById(R.id.imageView);
        iv.setImageResource(imageResource);
        setCancelable(false);
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

// > here i wanted to set image which i click on grid view

        return view;
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId()==R.id.button)
        {
            dismiss();
        }
    }
    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;

    }
}
