package com.example.myapplication.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.Adapter.CustomGridAdapter;
import com.example.myapplication.Dialog.BottomSheetDialog;
import com.example.myapplication.Dialog.mydialog;
import com.example.myapplication.R;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment {

    ImageView imageclear;
    SearchView searchview;
    ImageView searchimage;
    CircleImageView pictureview;
    //   CircleImageView pictureview;
    private static TextView text_v;
    private static RatingBar rating_b;


    GridView grid;
    String[] web = {
            "Grateres - Cookies and Cream","Grateres - Madagascar Vanilla Bean","Grateres - Black Cherry Chocolate Crip","Grateres - Mint Chocolate Chip","Grateres - Mint Cookies and Cream","Grateres - Cookies and Strawberry"




    } ;

    int[] imageId = {
            R.drawable.banana,
            R.drawable.vanilla,
            R.drawable.coffee,
            R.drawable.blackcherry,
            R.drawable.chocloate,
            R.drawable.chocolatechip
    };




    public ChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_chat, container, false);
        rating_b = (RatingBar)v.findViewById(R.id.ratingBar);
       // text_v = (TextView)v.findViewById(R.id.textView);
        listenerForRatingBar();
        GridView gridview = (GridView)v.findViewById(R.id.gridview);
        gridview.setAdapter(new CustomGridAdapter(getActivity(),web, imageId));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //   Toast.makeText(MainActivity.this, "You Clicked at " +web[+     position], Toast.LENGTH_SHORT).show();
                mydialog dialog = new mydialog();
                int imageResource = imageId[position];
                dialog.setImageResource(imageResource);

                dialog.show(getFragmentManager(),"my_dialog");
            }
        });
        pictureview =(CircleImageView)v.findViewById(R.id.pictureView);
        pictureview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BottomSheetDialog().show(getFragmentManager(), "Dialog");
            }
        });

        return  v;
    }
    private void listenerForRatingBar() {


        rating_b.setOnRatingBarChangeListener(
                new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        // text_v.setText(String.valueOf(rating));
                        Toast.makeText(getActivity(),String.valueOf(rating),Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
}
