package com.example.myapplication.MainActivity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.Adapter.CustomGridAdapter;
import com.example.myapplication.Adapter.PageAdapter;
import com.example.myapplication.Dialog.mydialog;
import com.example.myapplication.Fragment.FavouriteFragment;
import com.example.myapplication.Fragment.Home;
import com.example.myapplication.Fragment.HomeFragment;
import com.example.myapplication.Fragment.LogoutFragment;
import com.example.myapplication.Fragment.MyOrderFragment;
import com.example.myapplication.Fragment.MyProfile;
import com.example.myapplication.Fragment.NotificationFragment;
import com.example.myapplication.Fragment.PaymentWalletFragment;
import com.example.myapplication.Fragment.SettingFragment;
import com.example.myapplication.Fragment.ShopingcartFragment;
import com.example.myapplication.Fragment.dashboardFragment;
import com.example.myapplication.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    PageAdapter pageAdapter;
    TabItem tabChats;
    TabItem tabStatus;
    TabItem tabCalls;
    ImageView imageclear;
    SearchView searchview;
    ImageView searchimage;
    CircleImageView pictureview;
 //   CircleImageView pictureview;
    private static TextView text_v;
    private static RatingBar rating_b;

    GridView grid;
    String[] web = {
            "Grateres - Cookies and Cream","Grateres - Madagascar Vanilla Bean","Grateres - Black Cherry Chocolate Crip","Grateres - Mint Chocolate Chip","Grateres - Mint Cookies and Cream","Grateres - Cookies and Strawberry"




    } ;

    int[] imageId = {
            R.drawable.banana,
            R.drawable.vanilla,
            R.drawable.coffee,
            R.drawable.blackcherry,
            R.drawable.chocloate,
            R.drawable.chocolatechip
    };



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    getSupportFragmentManager().beginTransaction().replace(R.id.framecontainB, new HomeFragment()).commit();
                    return true;
                case R.id.navigation_dashboard:
                    getSupportFragmentManager().beginTransaction().replace(R.id.framecontainB, new dashboardFragment()).commit();
                  //  mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    getSupportFragmentManager().beginTransaction().replace(R.id.framecontainB, new NotificationFragment()).commit();
                  //  mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tabLayout = findViewById(R.id.tablayout);
        tabChats = findViewById(R.id.tabChats);
        tabStatus = findViewById(R.id.tabStatus);
        tabCalls = findViewById(R.id.tabCalls);
        viewPager = findViewById(R.id.viewPager);

        pageAdapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this,
                                R.color.colorAccent));
                    }
                } else if (tab.getPosition() == 2) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this,
                                android.R.color.darker_gray));
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this,
                                R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });














        BottomNavigationView navView = findViewById(R.id.nav_view);
       // mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        searchimage = (ImageView)findViewById(R.id.searchimage);
        searchview =(SearchView) findViewById(R.id.search_bar);
        searchimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchview.setVisibility(View.VISIBLE);
                imageclear.setVisibility(View.VISIBLE);
            }
        });

        listenerForRatingBar();

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new CustomGridAdapter(this,web, imageId));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //   Toast.makeText(MainActivity.this, "You Clicked at " +web[+     position], Toast.LENGTH_SHORT).show();
                mydialog dialog = new mydialog();
                int imageResource = imageId[position];
                dialog.setImageResource(imageResource);

                dialog.show(getSupportFragmentManager(),"my_dialog");
            }
        });

        pictureview =(CircleImageView) findViewById(R.id.pictureView);
        pictureview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new com.example.myapplication.Dialog.BottomSheetDialog().show(getSupportFragmentManager(),"Dialog");
            }
        });
        imageclear = (ImageView)findViewById(R.id.clearimage);
        imageclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchview.setVisibility(View.GONE);
                imageclear.setVisibility(View.GONE);
            }
        });



        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view1);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }


    private void listenerForRatingBar() {
        rating_b = (RatingBar) findViewById(R.id.ratingBar);
       // text_v = (TextView)findViewById(R.id.textView);

        rating_b.setOnRatingBarChangeListener(
                new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        // text_v.setText(String.valueOf(rating));
                        Toast.makeText(MainActivity.this,String.valueOf(rating),Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        switch (item.getItemId()){
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.framecontainA, new Home()).commit();
                break;
            case R.id.nav_gallery:
                getSupportFragmentManager().beginTransaction().replace(R.id.framecontainA, new ShopingcartFragment()).commit();
                break;
            case R.id.nav_slideshow:
                getSupportFragmentManager().beginTransaction().replace(R.id.framecontainA, new MyOrderFragment()).commit();
                break;

            case R.id.nav_tools:
                getSupportFragmentManager().beginTransaction().replace(R.id.framecontainA, new FavouriteFragment()).commit();
                break;
            case R.id.nav_home1:
                getSupportFragmentManager().beginTransaction().replace(R.id.framecontainA, new MyProfile()).commit();
                //Toast.makeText(this,"Share",Toast.LENGTH_LONG).show();
                break;

            case R.id.nav_gallery2:
                getSupportFragmentManager().beginTransaction().replace(R.id.framecontainA, new SettingFragment()).commit();
                //Toast.makeText(this,"Send",Toast.LENGTH_LONG).show();
                break;

            case R.id.nav_slideshow3:
                getSupportFragmentManager().beginTransaction().replace(R.id.framecontainA, new PaymentWalletFragment()).commit();
                break;

            case R.id.nav_tools4:
                getSupportFragmentManager().beginTransaction().replace(R.id.framecontainA, new LogoutFragment()).commit();
                break;
        }





        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
